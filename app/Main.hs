module Main where

import Control.Applicative ((<|>))
import System.Directory (getHomeDirectory)
import System.FilePath ((</>))
import Text.Parsec (Parsec, eof, many, newline, noneOf, parse, string, try)
import Text.PrettyPrint.Boxes (hsep, left, printBox, text, vcat)

data Book = Book
  { title :: String
  , author :: String
  }

main :: IO ()
main = do
  homeDirectory <- getHomeDirectory
  let path = homeDirectory </> "books.md"
  contents <- readFile path
  case parse booksParser path contents of
    Right books -> printBooks books
    Left error -> print error

booksParser :: Parsec String u [Book]
booksParser = do
  many bookParser <* eof

bookParser :: Parsec String u Book
bookParser =
  Book
    <$> (string "* *"
           *> many ((try (string "\n  " >> return ' ')) <|> noneOf "*"))
    <*> (string "* by "
           *> many ((try (string "\n  " >> return ' ')) <|> noneOf "\n")
           <* newline)

printBooks :: [Book] -> IO ()
printBooks books =
  let column field = vcat left $ map (text . field) books
   in printBox $ hsep 2 left [column title, column author]
